import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div>
      This is Homepage.
      <br />
      <Link to="/product">Check Products Here!!</Link>
    </div>
  );
};

export default Home;
