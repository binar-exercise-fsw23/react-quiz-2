import { useEffect, useState } from "react";
import axios from "axios";
import ReactPaginate from "react-paginate";

import ProductCard from "./components/ProductCard";

const API = "https://api.lemonilo.com/v1/";
const PRODUCT_POPULAR = "product/popular?";

const Products = (query = "") => {
  const [products, setProducts] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPage, setTotalPage] = useState(1);

  const fetchProducts = async (query) => {
    try {
      const { data } = await axios.get(API + PRODUCT_POPULAR + query);
      setProducts(data?.data || []);
      setCurrentPage(data?.currentPage);
      setTotalPage(data?.totalPages);
    } catch (error) {
      // silent e
    }
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  useEffect(() => {
    fetchProducts(`page=${currentPage}`);
  }, [currentPage]);

  const handlePageClick = (value) => {
    setCurrentPage(value.selected + 1);
  };

  return (
    <>
      <ProductCard products={products} />
      <div className="d-flex justify-content-end">
        <ReactPaginate
          breakLabel="..."
          nextLabel=">"
          onPageChange={handlePageClick}
          pageRangeDisplayed={5}
          pageCount={totalPage}
          previousLabel="<"
          pageClassName="page-item"
          pageLinkClassName="page-link"
          previousClassName="page-item"
          previousLinkClassName="page-link"
          nextClassName="page-item"
          nextLinkClassName="page-link"
          breakClassName="page-item"
          breakLinkClassName="page-link"
          containerClassName="pagination"
          activeClassName="active"
          renderOnZeroPageCount={null}
        />
      </div>
    </>
  );
};

export default Products;
