import { Card, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

const ProductCard = (props) => {
  const { products } = props;
  return (
    <Row xs={1} md={4} className="d-flex flex-wrap">
      {products.map((product, index) => {
        return (
          <Col key={index}>
            <Card className="m-2" style={{ width: "14.88rem" }}>
              <Link
                className="text-decoration-none text-dark"
                to={`${product.slug}`}
              >
                <Card.Img variant="top" src={product.photoUrl} />
                <Card.Body>
                  <span className="text-secondary">
                    {product.merchant.name}
                  </span>
                  <Card.Title className="h3">{product.name}</Card.Title>
                  <Card.Text className="text-secondary h6">
                    {Math.round(product.netWeight * 1000)} gr
                  </Card.Text>
                  <span
                    style={{
                      textDecoration: "line-through",
                      color: product.gimmickPrice ? "#979797" : "#000000",
                    }}
                  >
                    Rp {product.gimmickPrice}
                  </span>
                  <br />
                  <span className="text-Dark fw-bold">Rp {product.price}</span>
                </Card.Body>
                <Card.Footer className="bg-success text-center">
                  Beli
                </Card.Footer>
              </Link>
            </Card>
          </Col>
        );
      })}
    </Row>
  );
};

export default ProductCard;
