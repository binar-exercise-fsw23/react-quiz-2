import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Button, Card } from "react-bootstrap";

const API = "https://api.lemonilo.com/v1/";
const PRODUCT = "product/";

const ProductDetail = (props) => {
  const [product, setProduct] = useState();
  const params = useParams();
  const { product_slug } = params;

  const getProductDetail = async () => {
    try {
      const { data } = await axios.get(API + PRODUCT + product_slug);
      return setProduct(data.data);
    } catch (e) {
      // silent e
    }
  };

  useEffect(() => {
    getProductDetail();
  }, []);

  console.log(product);

  return (
    <Card>
      <Card.Header>Featured</Card.Header>
      <Card.Body>
        <Card.Title>Special title treatment</Card.Title>
        <Card.Text>
          With supporting text below as a natural lead-in to additional content.
        </Card.Text>
        <Button variant="primary">Go somewhere</Button>
      </Card.Body>
    </Card>
  );
};

export default ProductDetail;
