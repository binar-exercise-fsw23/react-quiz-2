import Home from "./pages/Home";
import Products from "./pages/Products/Products";
import ProductDetail from "./pages/ProductDetail/ProductDetail";

const routes = [
  {
    path: "/",
    page: <Home />,
  },
  {
    path: "product",
    page: <Products />,
  },
  {
    path: "product/:product_slug",
    page: <ProductDetail />,
  },
];

export default routes;
