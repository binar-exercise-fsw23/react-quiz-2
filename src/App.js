import { Container } from "react-bootstrap";
import { Routes, Route } from "react-router-dom";

import Header from "./components/Header/Header";
import TopNav from "./components/TopNav/TopNav";
import routes from "./routes";

const NotFound = () => {
  return <div>Not Found</div>;
};

const App = () => {
  return (
    <div className="sticky-top">
      <TopNav />
      <Header />
      <Container>
        <Routes>
          {routes.map((route) => {
            return (
              <Route key={route.path} path={route.path} element={route.page} />
            );
          })}
          <Route path="*" element={<NotFound />} />
        </Routes>
      </Container>
    </div>
  );
};

export default App;
