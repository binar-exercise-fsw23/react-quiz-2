import Breadcrumb from "react-bootstrap/Breadcrumb";
import "./NavBreadcrumb.css";

function NavBreadcrumb() {
  return (
    <Breadcrumb>
      <Breadcrumb.Item className="text-color-secondary">
        <i className="bi bi-house-door-fill"></i>
      </Breadcrumb.Item>
      <Breadcrumb.Item id="PalingLaris">Paling laris</Breadcrumb.Item>
    </Breadcrumb>
  );
}

export default NavBreadcrumb;
