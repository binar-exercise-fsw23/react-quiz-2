import { Container, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";

import NavListClient from "./components/NavListClient";
import NavListProduct from "./components/NavListProduct";
import Search from "./components/Search";

const Header = () => {
  return (
    <Navbar bg="white" className="border-bottom" expand="lg">
      <Container fluid>
        <Link className="text-decoration-none" to="/">
          <Navbar.Brand>Navbar scroll</Navbar.Brand>
        </Link>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <NavListProduct />
        </Navbar.Collapse>
        <Search />
        <NavListClient />
      </Container>
    </Navbar>
  );
};

export default Header;
