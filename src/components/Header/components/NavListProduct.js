import { Nav } from "react-bootstrap";
import DropDownMenu from "./DropDownMenu";

const NavListProduct = () => {
  return (
    <>
      <Nav
        className="me-auto my-2 my-lg-0"
        style={{ maxHeight: "100px" }}
        navbarScroll
      >
        <DropDownMenu href="#action1">Kategori</DropDownMenu>
        <Nav.Link href="#action2">Promo</Nav.Link>
        <Nav.Link href="#action3">Life</Nav.Link>
      </Nav>
    </>
  );
};

export default NavListProduct;
