import { Nav } from "react-bootstrap";

const AddToCart = () => {
  return (
    <Nav.Link href="#">
      <i className="bi bi-cart4"></i>
    </Nav.Link>
  );
};

export default AddToCart;
