import { Nav } from "react-bootstrap";

const NavAuth = () => {
  const auths = ["Masuk", "Daftar"];
  return (
    <>
      <Nav.Link href="#">{auths[0]}</Nav.Link>
      <Nav.Link href="#">{auths[1]}</Nav.Link>
    </>
  );
};

export default NavAuth;
