import { Nav } from "react-bootstrap";

const NavNotif = () => {
  return (
    <Nav.Link href="#">
      <i className="link-secondary bi bi-bell-fill"></i>
    </Nav.Link>
  );
};

export default NavNotif;
