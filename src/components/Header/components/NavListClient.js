import { Nav } from "react-bootstrap";
import AddToCart from "./AddToCart";
import NavAuth from "./NavAuth";
import NavNotif from "./NavNotif";

const NavListClient = () => {
  return (
    <>
      <Nav>
        <AddToCart className="me-auto"></AddToCart>
        <NavNotif className="me-auto"></NavNotif>
        <NavAuth className="me-auto"></NavAuth>
      </Nav>
    </>
  );
};

export default NavListClient;
