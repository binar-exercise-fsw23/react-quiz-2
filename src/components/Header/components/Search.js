import { Form, InputGroup } from "react-bootstrap";

const Search = () => {
  return (
    <InputGroup>
      <InputGroup.Text id="basic-addon1">
        <i className="bi bi-search"></i>
      </InputGroup.Text>
      <Form.Control
        type="search"
        placeholder="Search"
        aria-label="Search"
        className="me-2"
        aria-describedby="basic-addon1"
      />
    </InputGroup>
  );
};

export default Search;
