import { NavDropdown } from "react-bootstrap";

const DropDownMenu = () => {
  const items = [
    "Mie",
    "Bahan Dapur",
    "Cemilan",
    "Brand Kolaborasi",
    "Join Reseller Program",
  ];
  return (
    <NavDropdown title="Kategori" id="navbarScrollingDropdown">
      {items.map((item, index) => {
        return (
          <NavDropdown.Item href="#" key={index}>
            {item}
          </NavDropdown.Item>
        );
      })}
    </NavDropdown>
  );
};

export default DropDownMenu;
