import { Container, Nav } from "react-bootstrap";

const TopNav = () => {
  return (
    <Container fluid style={{ backgroundColor: "#E5F4D6" }}>
      <Nav className="d-flex justify-content-between">
        <Nav.Item>
          <Nav.Link>Download Aplikasi Lemonilo</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link>Butuh Bantuan? (+62)21 4020 0788</Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link>
            Diskon sebesar IDR 50000 dengan kupon BELANJASEHAT untuk pelanggan
            baru
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link>Kenapa Belanja di Lemonilo?</Nav.Link>
        </Nav.Item>
      </Nav>
    </Container>
  );
};

export default TopNav;
